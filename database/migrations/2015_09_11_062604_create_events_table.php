<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('events', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('title');$table->string('type');$table->text('description');$table->string('location');$table->string('url');$table->string('timezone');$table->string('date');$table->string('time');$table->string('enddate');$table->string('endtime');
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
    }
}